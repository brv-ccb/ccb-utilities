<?php

// Include composer autoloader
require dirname(__DIR__) . '/vendor/autoload.php';

use GuzzleHttp\Client;

$credentials = array('jeremy','jpapi15');
$base_uri = 'https://brv.ccbchurch.com';

$ccbClient = new Client([
	'base_uri' => $base_uri,
	'auth' => $credentials,
	'Accept' => 'application/xml',
]);

function get_participants($groupId)
{
	global $ccbClient;

	$response = $ccbClient->get('api.php',['query'=>['srv'=>'group_participants','id'=>$groupId]]);
	$xmlResult = simplexml_load_string($response->getBody()->getContents());

	$participantIds = array();
	// var_dump($xmlResult);
	// var_dump($xmlResult->response->groups->group->participants);
	foreach($xmlResult->response->groups->group->participants AS $participants) {
		// var_dump($participant->participant);
		foreach ($participants->participant AS $participant) {
			// var_dump($participant);
			// var_dump((int) $participant->attributes()->id);
			$participantIds[] = (int) $participant->attributes()->id;
		}
		
	}
	$jsonResult = json_encode($xmlResult);
	$result = json_decode($jsonResult);
	$participants = $result->response->groups->group->participants->participant;
	// var_dump($participantIds);
	foreach ($participants AS $key => &$participant) {
		$participant->id = $participantIds[$key];
	}
	// var_dump($participants);
	return $participants;
}

function get_availability($personId) {
	
	global $ccbClient;

	$response = $ccbClient->get('api.php',['query'=>['srv'=>'individual_availability','id'=>$personId]]);
	$xmlResult = simplexml_load_string($response->getBody()->getContents());
	$jsonResult = json_encode($xmlResult);
	$result = json_decode($jsonResult)->response;
	
	$tmp = [];
	
	$tmp['name'] = $result->individuals->individual->full_name;
	if (isset($result->individuals->individual->unavailable_dates->date))
	$tmp['unavailable'] = $result->individuals->individual->unavailable_dates->date;
	if (isset($result->individuals->individual->accepted_requests->date))
	$tmp['accepted_requests'] = $result->individuals->individual->accepted_requests->date;
	

	return $tmp;
}

function getSundays($y, $m)
{
    return new DatePeriod(
        new DateTime("first sunday of $y-$m"),
        DateInterval::createFromDateString('next sunday'),
        new DateTime("last day of $y-$m")
    );
}



$participants = get_participants(53);

// var_dump($participants);

$availability = array();
foreach ($participants AS $participant) {
	$avail = get_availability($participant->id);
	$availability[$participant->id] = $avail;
}


$months = [9,10,11,12];


?>

<html>
<body>
<table>
<tr>
	<th>Date</th>
	<?php foreach($participants AS $participant): ?>
	<th><?php echo $participant->name; ?></th>
</tr>
<?php foreach($months AS $month):?>
	<?php foreach (getSundays(2016, $month) as $sunday): ?>
	    <tr>
	    <td><?php echo $sunday->format("l, Y-m-d\n"); ?></td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
	    <td></td>
			<td></td>
	  	</tr>
	<?php endforeach; ?>
<?php endforeach; ?>
</table>
</body>
</html>
