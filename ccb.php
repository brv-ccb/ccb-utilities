<?php

namespace CcbApi;

// Include composer autoloader
require __DIR__ . '/vendor/autoload.php';

use GuzzleHttp\Client;

class ccbApi {
	
	protected $credentials = array('jeremy','jpapi15');
	protected $base_uri = 'https://brv.ccbchurch.com';

	protected function __constructor()
	{
		$this->ccbClient = new Client([
			'base_uri' => $this->base_uri,
			'auth' => $this->credentials,
			'Accept'     => 'application/xml',
		]);
	}

	protected function get_participants($groupId)
	{
		$response = $this->ccbClient->get('api.php',['query'=>['srv'=>'group_participants','id'=>$groupId]]);
		$xmlResult = simplexml_load_string($response->getBody()->getContents());

		$participantIds = array();
		// var_dump($xmlResult);
		// var_dump($xmlResult->response->groups->group->participants);
		foreach($xmlResult->response->groups->group->participants AS $participants) {
			// var_dump($participant->participant);
			foreach ($participants->participant AS $participant) {
				// var_dump($participant);
				// var_dump((int) $participant->attributes()->id);
				$participantIds[] = (int) $participant->attributes()->id;
			}
			
		}
		$jsonResult = json_encode($xmlResult);
		$result = json_decode($jsonResult);
		$participants = $result->response->groups->group->participants->participant;
		// var_dump($participantIds);
		foreach ($participants AS $key => &$participant) {
			$participant->id = $participantIds[$key];
		}
		// var_dump($participants);
		return $participants;
	}

	protected function get_availability($personid) {
		
		$response = $this->ccbClient->get('api.php',['query'=>['srv'=>'individual_availability','id'=>$personId]]);
		$xmlResult = simplexml_load_string($response->getBody()->getContents());


		return $result;
	}


}





